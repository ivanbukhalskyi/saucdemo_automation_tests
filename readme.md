#Project
A sample test-suite which runs against 
Swaglabs' SAUCDEMO site. The following 
aspects of functionality undergo the 
testing: login, adding/removing items 
to/from cart. 

## Prerequisites
* Make sure that Python and PIP are installed
before continuing further. Author relied on 
Python3 and PIP3 accordingly.
* Chrome browser should be installed 
as it's being set up as a default one in
tests' configuration.

## Install guide
```
pip3 install -r requirements.txt 
```
## Run guide
* To execute tests run the following in the 
terminal via the project's root folder:
```
pytest
```
* Junit and AllureHTML reporters are being
used. To generate a nice HTML report based on
the latest test run this withing the project's
root:
```
allure serve ./report
```
* To run PEP8 compliance checker execute while
being in the root:
```
pycodestyle .
```
## Useful links
- https://playwright.dev
- https://github.com/microsoft/playwright-python
- https://pycodestyle.pycqa.org/en
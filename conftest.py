from typing import Generator, Callable, Any

import pytest
from playwright.sync_api import sync_playwright, Playwright, Browser, BrowserContext, Page


@pytest.fixture(scope="session")
def playwright() -> Generator[Playwright, None, None]:
    pw = sync_playwright().start()
    yield pw
    pw.stop()


@pytest.fixture(scope="session")
def browser_type_launch_args(pytestconfig: Any) -> dict:
    launch_options = {}
    headed_option = pytestconfig.getoption("--headed")
    if headed_option:
        launch_options["headless"] = True
    return launch_options


@pytest.fixture(scope="session")
def launch_browser(playwright: Playwright, browser_type_launch_args: dict) -> Callable[..., Browser]:
    def launch(**kwargs: dict) -> Browser:
        launch_options = {**browser_type_launch_args, **kwargs}
        browser = playwright.chromium.launch(**launch_options)
        return browser

    return launch


@pytest.fixture(scope="session")
def browser(launch_browser: Callable[[], Browser]) -> Generator[Browser, None, None]:
    browser = launch_browser()
    yield browser
    browser.close()


@pytest.fixture(scope="session")
def browser_context_args(pytestconfig: Any, playwright: Playwright) -> dict:
    context_args = {}
    context_args["base_url"] = 'https://www.saucedemo.com/'
    context_args['viewport'] = {'width': 1920, 'height': 1080}
    return context_args


@pytest.fixture
def context(browser: Browser, browser_context_args: dict) -> Generator[BrowserContext, None, None]:
    context = browser.new_context(**browser_context_args)
    yield context
    context.close()


@pytest.fixture
def page(context: BrowserContext) -> Generator[Page, None, None]:
    page = context.new_page()
    yield page


def pytest_addoption(parser):
    parser.addoption('--headed', action='store', default=True)

import allure

from page_elements.cart_elements import CartItemElements
from page_objects.base_page import BasePage


class CartPage(BasePage):
    PATH = 'cart.html'

    def __init__(self, page):
        super().__init__(page)
        self.page = page
        self.cart_item_elements = lambda title_of_item: CartItemElements(self.page, title_of_item=title_of_item)

    def open_page(self) -> 'CartPage':
        super().open()
        return self

    def get_all_products(self) -> list[CartItemElements]:
        products = []

        for locator in self.page.locator('//div[@class="inventory_item_name"]').all():
            products.append(self.cart_item_elements(locator.inner_text()))

        return products

    def remove_cart_items(self, title_of_items: str | list[str]) -> 'CartPage':
        if isinstance(title_of_items, str):
            title_of_items = [title_of_items]

        for title_of_item in title_of_items:
            with allure.step(f'Click "Remove" on "{title_of_item}"'):
                self.cart_item_elements(title_of_item).remove_button \
                    .click()
        return self

import allure
from hamcrest import assert_that, equal_to
from playwright.sync_api import Page

from page_elements.base_elements import BaseElements


class BasePage:
    PATH = ''

    def __init__(self, page: Page):
        self.page = page
        self.base_elements = BaseElements(self.page)

    def open(self) -> 'BasePage':
        with allure.step(f'Open {self.PATH}'):
            self.page.goto(self.PATH)
        return self

    def verify_page_title(self, title: str) -> 'BasePage':
        with allure.step('Verify page title'):
            assert_that(self.base_elements.page_title.inner_text(), equal_to(title))
        return self

    def open_cart(self):
        with allure.step('Open cart'):
            self.base_elements.empty_cart.click()
        return self

import allure
from hamcrest import assert_that, equal_to

from page_elements.login_elements import LoginElements
from page_objects.base_page import BasePage


class LoginPage(BasePage):
    PATH = ''

    def __init__(self, page):
        super().__init__(page)
        self.page = page
        self.login_elements = LoginElements(self.page)

    def open_page(self) -> 'LoginPage':
        super().open()
        return self

    def login(self, username: str, password: str) -> 'LoginPage':
        with allure.step(f'Login as username: "{username}", password: "**********"'):
            self.login_elements.username_field.fill(username)
            self.login_elements.password_field.fill(password)
            self.login_elements.login_button.click()
        return self

    def verify_login_error(self, error_message: str) -> 'LoginPage':
        with allure.step(f'Verify error message "{error_message}" present'):
            self.login_elements.error_icon(self.login_elements.username_field).is_visible()
            self.login_elements.error_icon(self.login_elements.password_field).is_visible()
            assert_that(self.login_elements.error_message.inner_text(), equal_to(error_message))
        return self

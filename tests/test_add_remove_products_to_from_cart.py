import random

import allure
import pytest
from hamcrest import assert_that, equal_to


@pytest.fixture
def add_products_to_cart(inventory_page, cart_page, open_inventory_page):
    items_added_to_card = random.sample(products := inventory_page.get_all_products(),
                                        k=random.randint(1, len(products)))

    with allure.step('Add products to cart'):
        inventory_page \
            .add_items_to_cart([locator.title.inner_text() for locator in items_added_to_card])

        assert_that(int(inventory_page.base_elements.cart_with_products.inner_text()),
                    equal_to(len(items_added_to_card)))

    return items_added_to_card


@allure.feature('Product Page')
@allure.title('The user can add the required number of products to the cart')
def test_user_can_add_the_required_number_of_products_to_the_cart(
        inventory_page, cart_page, add_products_to_cart
):
    with allure.step('#1 Verify product are exist in the cart'):
        expected_product_characteristic = [(i.title.inner_text(), i.price.inner_text(),) for i in add_products_to_cart]

        inventory_page \
            .open_cart() \
            .verify_page_title('YOUR CART')

        actual_card_items = cart_page.get_all_products()
        actual_card_items_product_characteristic = [(i.title.inner_text(), i.price.inner_text()) for i in
                                                    actual_card_items]

        for actual, expected in zip(actual_card_items_product_characteristic, expected_product_characteristic):
            assert_that(actual, equal_to(expected))


@allure.feature('Product Page')
@allure.title('The user can remove the selected products from the cart on "Product Page"')
def test_user_can_remove_the_selected_products_from_the_cart_on_product_page(
        inventory_page, cart_page, add_products_to_cart
):
    items_to_remove = random.sample(products := add_products_to_cart, k=random.randint(1, len(products)))
    expected_product_characteristic = [(i.title.inner_text(), i.price.inner_text(),) for i in add_products_to_cart
                                       if i not in items_to_remove]

    with allure.step('#1 Remove the selected products from the cart on "Product Page"'):
        inventory_page \
            .remove_cart_items([locator.title.inner_text() for locator in items_to_remove])

    with allure.step('#2 Verify product was removed from the cart'):
        if inventory_page.base_elements.empty_cart.is_hidden():
            assert_that(int(inventory_page.base_elements.cart_with_products.inner_text()),
                        equal_to(len(expected_product_characteristic)))

        inventory_page \
            .open_cart() \
            .verify_page_title('YOUR CART')

        actual_card_items = cart_page.get_all_products()
        actual_card_items_product_characteristic = [(i.title.inner_text(), i.price.inner_text()) for i in
                                                    actual_card_items]

        for actual, expected in zip(actual_card_items_product_characteristic, expected_product_characteristic):
            assert_that(actual, equal_to(expected))


@allure.feature('Cart Page')
@allure.title('The user can remove the selected products from the cart on "Cart Page"')
def test_user_can_remove_the_selected_products_from_the_cart_on_cart_page(
        inventory_page, cart_page, add_products_to_cart
):
    inventory_page \
        .open_cart()

    items_to_remove = random.sample(products := cart_page.get_all_products(), k=random.randint(1, len(products)))
    expected_product_characteristic = [(i.title.inner_text(), i.price.inner_text(),) for i in products
                                       if i.title.inner_text() not in [r.title.inner_text() for r in items_to_remove]]

    with allure.step('#1 Remove the selected products from the cart on "Cart Page"'):
        cart_page \
            .verify_page_title('YOUR CART') \
            .remove_cart_items([locator.title.inner_text() for locator in items_to_remove])

    with allure.step('#2 Verify product was removed from the cart'):
        if cart_page.base_elements.empty_cart.is_hidden():
            assert_that(int(cart_page.base_elements.cart_with_products.inner_text()),
                        equal_to(len(expected_product_characteristic)))

        actual_card_items = cart_page.get_all_products()
        actual_card_items_product_characteristic = [(i.title.inner_text(), i.price.inner_text()) for i in
                                                    actual_card_items]

        for actual, expected in zip(actual_card_items_product_characteristic, expected_product_characteristic):
            assert_that(actual, equal_to(expected))

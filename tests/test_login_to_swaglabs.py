import allure
import pytest

from config import STANDARD_USER, LOCKED_OUT_USER, PROBLEM_USER, PERFORMANCE_GITCH_USER, USERS_PASSWORD


@allure.feature('Login Page')
@allure.title('The user can successfully log in with valid credentials')
@pytest.mark.parametrize(
    'username, password',
    [(STANDARD_USER, USERS_PASSWORD),
     (PROBLEM_USER, USERS_PASSWORD),
     (PERFORMANCE_GITCH_USER, USERS_PASSWORD)],
    ids=['Valid credentials',
         'Valid credentials',
         'Valid credentials'])
def test_user_can_successfully_log_in_with_valid_credentials(login_page, open_login_page, username, password):
    with allure.step('#1 Sign in "Swaglab"'):
        login_page \
            .login(username, password) \
            .verify_page_title('PRODUCTS')


@allure.feature('Login Page')
@allure.title('The locked user can`t log in with valid credentials')
def test_locked_user_can_not_log_in_with_valid_credentials(login_page, open_login_page):
    with allure.step('#1 Sign in "Swaglab"'):
        login_page \
            .login(LOCKED_OUT_USER, USERS_PASSWORD) \
            .verify_login_error('Epic sadface: Sorry, this user has been locked out.')


username_is_required = 'Epic sadface: Username is required'
password_is_required = 'Epic sadface: Password is required'
username_and_password_do_not_match = 'Epic sadface: Username and password do not match any user in this service'


@allure.feature('Login Page')
@allure.title('The user can`t log in with empty or valid credentials')
@pytest.mark.parametrize(
    'username, password, expected_error_message',
    [(STANDARD_USER, 'qwerty123', username_and_password_do_not_match),
     ('invalid_username', USERS_PASSWORD, username_and_password_do_not_match),
     ('invalid_username', 'qwerty123', username_and_password_do_not_match),
     (STANDARD_USER, '', password_is_required),
     ('', USERS_PASSWORD, username_is_required)],
    ids=['Valid username, invalid password',
         'Invalid username, valid password',
         'Invalid username, invalid password',
         'Valid username, empty password',
         'Empty password, valid username'])
def test_user_can_not_log_in_with_empty_or_valid_credentials(
        login_page, open_login_page, username, password, expected_error_message
):
    with allure.step('#1 Sign in "Swaglab"'):
        login_page \
            .login(username, password) \
            .verify_login_error(expected_error_message)

import pytest
from playwright.sync_api import Page

from page_objects.cart_page import CartPage
from page_objects.inventory_page import InventoryPage
from page_objects.login_page import LoginPage


@pytest.fixture
def login_page(page: Page) -> LoginPage:
    return LoginPage(page)


@pytest.fixture
def inventory_page(page: Page) -> InventoryPage:
    return InventoryPage(page)


@pytest.fixture
def cart_page(page: Page) -> CartPage:
    return CartPage(page)


@pytest.fixture
def open_login_page(login_page):
    login_page \
        .open_page()


@pytest.fixture
def login_to_swaglabs(login_page, open_login_page):
    login_page \
        .login('standard_user', 'secret_sauce') \
        .verify_page_title('PRODUCTS')


@pytest.fixture
def open_inventory_page(login_to_swaglabs):
    pass

from playwright.sync_api import Page


class LoginElements:
    def __init__(self, page: Page):
        self.page = page

        self.login_page_logo = self.page.locator('//div[@class="login_logo"]')

        self.username_field = self.page.locator('//input[@placeholder="Username"]')
        self.password_field = self.page.locator('//input[@placeholder="Password"]')
        self.login_button = self.page.locator('//input[@name="login-button"]')

        self.error_icon = lambda field: field.locator('//svg[@class="svg-inline--fa fa-times-circle fa-w-16 error_icon"]')
        self.error_message = self.page.locator('//div[@class="error-message-container error"]/h3')

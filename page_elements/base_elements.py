from playwright.sync_api import Page


class BaseElements:
    def __init__(self, page: Page):
        self.page = page

        self.page_title = self.page.locator('//div[@class="header_secondary_container"]/span')
        self.empty_cart = self.page.locator('//a[@class="shopping_cart_link"]')
        self.cart_with_products = self.empty_cart.locator('//span[@class="shopping_cart_badge"]')

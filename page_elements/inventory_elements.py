from playwright.sync_api import Page


class CartItemElements:
    def __init__(self, page: Page, title_of_item: str):
        self.page = page
        self.root_element = self.page.locator(f'//div[text()="{title_of_item}"]'
                                              f'//ancestor::div[@class="inventory_item"]')

        self.title = self.root_element.locator('//div[@class="inventory_item_name"]')
        self.price = self.root_element.locator('//div[@class="inventory_item_price"]')
        self.button = lambda text: self.root_element.locator(f'//button[text()="{text}"]')

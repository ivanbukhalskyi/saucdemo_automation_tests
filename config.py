from environs import Env

env = Env()
env.read_env(verbose=True)

STANDARD_USER = env.str('STANDARD_USER', None)
LOCKED_OUT_USER = env.str('LOCKED_OUT_USER', None)
PROBLEM_USER = env.str('PROBLEM_USER', None)
PERFORMANCE_GITCH_USER = env.str('PERFORMANCE_GITCH_USER', None)

USERS_PASSWORD = env.str('USERS_PASSWORD', None)
